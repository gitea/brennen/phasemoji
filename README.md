phasemoji
=========

This is a trivial wrapper around Samir Shah's [php-moon-phase][] which prints
an emoji roughly corresponding to the current moon phase.

[php-moon-phase]: https://github.com/solarissmoke/php-moon-phase/

I was originally pointed to this library by Vijay's [moon][].

[moon]: https://github.com/vijinho/moon

It has no features and is probably wrong.

synopsis
--------

```sh
./phasemoji 
🌑
```

installing
----------

```sh
git clone http://code.p1k3.com/gitea/brennen/phasemoji.git
cd phasemoji
# you may need to install composer: https://getcomposer.org/download/
composer install

# or possibly:
php composer.phar install
```

author
------

Brennen Bearnes  
https://p1k3.com/

copying
-------

Samir Shah's php-moon-phase is MIT, so for this bit of boilerplate I guess
we'll go with the same.
